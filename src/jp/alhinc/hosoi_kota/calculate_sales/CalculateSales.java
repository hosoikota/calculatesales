package jp.alhinc.hosoi_kota.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {
	public static void main(String[] args){
		//No1-2：コマンドライン引数が渡されてない場合
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//コードと支店名のmapName、コードと合計金額のmapSales
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		//支店定義ファイルを読み込み、マップに入れるメソッド追加
		if(! readNameFile(args[0], "branch.lst", "支店定義ファイル", "^[0-9]{3}$", nameMap, salesMap)){
			return;
		}

		//売上ファイル読み込みメソッド呼び出し
		if(! readSalesFile(args[0], "売上ファイル名", "^[0-9]{8}\\.rcd$", 10, nameMap, salesMap)){
			return;
		}

		//集計結果をファイルに出力するメソッド
		if(! writeResult(args[0], "branch.out", nameMap, salesMap)){
			return;
		}
	}

	//支店定義ファイルのメソッド
	public static boolean readNameFile(String directoryPath, String filePath, String fileName, String fileFormat, HashMap<String, String> mapName, HashMap<String, Long> mapSales){
		BufferedReader br = null;
		try{
			File f = new File(directoryPath, filePath);

			//No3：支店定義ファイルが存在しない場合
			if(! f.exists()){
				System.out.println(fileName +  "が存在しません");
				return false;
			}
			br = new BufferedReader(new FileReader(f));
			String line;
			while((line = br.readLine()) != null){
				String[] data = line.split(",");
				//No4-13：支店定義ファイルのフォーマットが不正の場合
				if(! data[0].matches(fileFormat) ||  data.length != 2){
					System.out.println(fileName + "のフォーマットが不正です");
					return false;
				}
				//支店定義ファイルの内容をmapNameに格納
				mapName.put(data[0],data[1]);
				//同時にmapSalesの支店コードに対応する売り上げ金額を初期化する
				mapSales.put(data[0],0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null){
				try {
					br.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//売上ファイルのメソッド
	public static boolean readSalesFile(String directoryPath, String fileName, String fileNameFormat, int saleLimit, HashMap<String, String> mapName, HashMap<String, Long> mapSales){
		BufferedReader br = null;
		// 売り上げファイルを読み込む
		try {
			//指定されたディレクトリ内のファイルをすべてfilesに格納
			File[] files = new File(directoryPath).listFiles();
			//売上ファイルを格納するためのリスト
			List<File> saleFiles = new ArrayList<File>();
			//ファイル名の連番を確認するため、数値化したファイル名を格納するためのリスト
			List<Integer> number = new ArrayList<Integer>();

			//指定されたファイルを探してそれを文字列リストに格納、それをマップに格納
			for(int i= 0; i < files.length; i++){
				//No16.No17.No18：のエラー確認
				if((files[i].getName()).matches(fileNameFormat)){
					//正規表現にマッチする名前がフォルダでない場合に売上ファイルとして格納
					//No15：エラーチェック
					if(files[i].isFile()){
						//売上ファイルをsaleFilesに格納
						saleFiles.add(files[i]);
						//売上ファイル名を数値にしたものをnumberに入れる
						number.add( Integer.parseInt(files[i].getName().substring(0, 8)));
					}
				}
			}
			//No14-18:ファイル名連番処理
			for(int i = 0; i < number.size() - 1; i++){
				if(number.get(i+1) - number.get(i) != 1 ){
					System.out.println(fileName + "が連番になっていません");
					return false;
				}
			}
			for(int i = 0; i < saleFiles.size(); i++){
				br = new BufferedReader(new FileReader(saleFiles.get(i)));

				String line;	//ファイルの中身を読み込む場所
				List<String> sales = new ArrayList<String>();		//ファイルの中身をリストとして格納する場所

				while((line = br.readLine()) != null){
					sales.add(line);
				}

				//No25.26売り上げファイルの中身が3行以上、1行以下である場合のエラー処理
				if(sales.size() != 2){
					System.out.println(saleFiles.get(i).getName() + "のフォーマットが不正です");
					return false;
				}
				//No19-23エラー処理
				if(! sales.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				//No24:売り上げファイルの支店コードが支店定義ファイルに存在しない場合のエラー処理
				if(! mapName.containsKey(sales.get(0))){
					System.out.println(saleFiles.get(i).getName() + "の支店コードが不正です");
					return false;
				}

				long sum = 0;
				//同じ支店コードの売り上げが来たときの売り上げ加算
				sum = mapSales.get(sales.get(0)) + Long.parseLong(sales.get(1));
				//合計金額が10桁を超えた場合、エラーを出す
				if(String.valueOf(sum).length() > saleLimit){
					System.out.println("合計金額が" + saleLimit +"桁を超えました");
					return false;
				}
				mapSales.put(sales.get(0), sum);
			}
		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally{
			if(br != null){
				try{
					br.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//集計結果出力メソッド
	public static boolean writeResult (String directoryPath, String filePath, HashMap<String, String> mapName, HashMap<String, Long> mapSales) {
		BufferedWriter bw = null;
		try {
			File newFile = new File(directoryPath,filePath);
			//ファイル作成
			newFile.createNewFile();

			FileWriter fw = new FileWriter(newFile);
			bw = new BufferedWriter(fw);

			for(Map.Entry<String, String> entry : mapName.entrySet()){
				bw.write(entry.getKey() + "," + entry.getValue() + "," + mapSales.get(entry.getKey()));
				bw.newLine();
			}
		} catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally{
			if(bw != null){
				try{
					bw.close();
				} catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
